﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField]
    public float speed = 100;

    private Rigidbody2D rb;
    private Animator animator;
    public VectorValue startingPosition;
    private Vector2 previousMovement;

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        transform.position = startingPosition.initialValue;
    }

    private void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        Vector2 movement = new Vector2(horizontal, vertical);

        rb.velocity = movement * speed * Time.deltaTime;

        if (movement != Vector2.zero)
        {
            // Lưu hướng di chuyển hiện tại
            previousMovement = movement;

            if (Mathf.Abs(horizontal) > Mathf.Abs(vertical))
            {
                // Di chuyển ngang (trái hoặc phải)
                if (horizontal > 0)
                    animator.Play("right");
                else
                    animator.Play("left");
            }
            else
            {
                // Di chuyển dọc (lên hoặc xuống)
                if (vertical > 0)
                    animator.Play("top");
                else
                    animator.Play("down");
            }
        }
        else
        {
            // Nếu không có di chuyển, sử dụng hướng di chuyển trước đó để xác định animation "Idle"
            if (previousMovement.x > 0)
                animator.Play("rightIdle");
            else if (previousMovement.x < 0)
                animator.Play("leftIdle");
            else if (previousMovement.y > 0)
                animator.Play("topIdle");
            else if (previousMovement.y < 0)
                animator.Play("downIdle");
        }
    }
}
