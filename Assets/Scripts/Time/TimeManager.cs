using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TimeManager : MonoBehaviour
{
    [Header("Date & Time Settings")]
    [Range(1, 28)]
    [SerializeField]
    private int DateInMonth;
    [Range(0,3)]
    [SerializeField]
    private int Season;
    [Range(1,99)]
    [SerializeField]
    private int Year;
    [Range(1,24)]
    [SerializeField]
    private int Hour;
    [Range(1,6)]
    [SerializeField]
    private int Minute;

    private DateTime dateTime;

    [Header("Tick Settings")]
    [SerializeField]
    private int TickMinutesIncrease = 10;
    [SerializeField]
    private float TimeBetweenTicks = 1;
    private float CurrentTimeBetweenTicks = 0;

    public static UnityAction<DateTime> OnDateTimeChanged;
    public static UnityAction OnNewDay;

    private void Awake()
    {
        //Log fields
        Debug.Log($"DateInMonth: {DateInMonth}");
        Debug.Log($"Season: {Season}");
        Debug.Log($"Year: {Year}");
        Debug.Log($"Hour: {Hour}");
        Debug.Log($"Minute: {Minute}");



        dateTime = new DateTime(DateInMonth, Season, Year, Hour, Minute * 10);

        var properties = dateTime.GetType().GetProperties();

        // Iterate over the properties and log them out.
        foreach (var property in properties)
        {
            Debug.Log($"{property.Name}: {property.GetValue(dateTime)}");
        }

    }

    private void Start()
    {
        OnDateTimeChanged?.Invoke(dateTime);
    }

    private void Update()
    {
        CurrentTimeBetweenTicks += Time.deltaTime;
        if (CurrentTimeBetweenTicks >= TimeBetweenTicks)
        {
            CurrentTimeBetweenTicks = 0;
            Tick();
        }
    }
    void Tick()
    {
        AdvanceTime();
    }
    void AdvanceTime()
    {
        dateTime.AdvanceMinutes(TickMinutesIncrease);
        OnDateTimeChanged?.Invoke(dateTime);
    }

    public int GetTotalDays()
    {
        return dateTime.TotalNumDays;
    }

   
    public struct DateTime
    {
        #region Fields
        private Days day;
        private int date;
        private int year;

        private int hour;
        private int minutes;

        private Season season;

        private int totalNumDays;
        private int totalNumWeeks;
        #endregion

        #region Properties
        public Days Day => day;
        public int Date => date;
        public int Year => year;

        public int Hour => hour;
        public int Minutes => minutes;

        public Season CurrentSeason => season;

        public int TotalNumDays => totalNumDays;
        public int TotalNumWeeks => totalNumWeeks;
        public int CurrentWeek => TotalNumWeeks % 16 == 0 ? 16 : TotalNumWeeks % 16;

        #endregion

        #region Constructors
        public DateTime(int date, int season, int year, int hour, int minutes)
        {
            day = (Days)(date % 7);
            if (day == 0) day = (Days)7;
            this.date = date;
            this.season = (Season)season;
            this.year = year;

            this.hour = hour;
            this.minutes = minutes;

            this.totalNumDays = date + (28 * (int)this.season) + (112 * (year - 1));

            this.totalNumWeeks = 1 + this.totalNumDays / 7;
        }
        #endregion

        #region Time Advancement
        public void AdvanceMinutes(int SecondsToAdvanceBy)
        {
            if (Minutes + SecondsToAdvanceBy >= 60)
            {
                minutes = (Minutes + SecondsToAdvanceBy) % 60;
                AdvanceHour();
            }
            else
            {
                minutes += SecondsToAdvanceBy;
            }
        }
        private void AdvanceHour()
        {
            if ((hour + 1) == 24)
            {
                hour = 0;
                AdvanceDay();
            }
            else
            {
                hour++;
            }
        }
        private void AdvanceDay()
        {
            OnNewDay.Invoke();

            //Update day
            day++;
            if (day > Days.Sunday)
            {//If a week passed
                day = Days.Monday;
                totalNumWeeks++;
            }

            //Update date
            date++;

            if (date % 29 == 0)
            {//If a month passed
                AdvanceSeason();
                date = 1;
            }
            totalNumDays++;

        }
        private void AdvanceSeason()
        {
            if (CurrentSeason == Season.ForthMonth)
            {
                season = Season.FirstMonth;
            }
            else
            {
                season++;
            }
        }
        private void AdvanceYear()
        {
            date = 1;
            year++;
        }
        #endregion

        #region Bool Checks
        public bool IsNight()
        {
            return hour > 18 || hour < 6;
        }

        public bool IsMonrning()
        {
            return hour >= 6 && hour <= 12;
        }
        public bool IsAfternoon()
        {
            return hour > 12 && hour < 18;
        }

        public bool IsWeekend()
        {
            return day > Days.Friday;
        }

        public bool IsParticularDayInWeek(Days day)
        {
            return this.day == day;
        }
        #endregion

        #region Key Dates
        public TimeManager.DateTime NewYearsDay(int year)
        {
            if (year == 0) year = 1;
            return new TimeManager.DateTime(1,(int)Season.FirstMonth,year,6,0);
        }
        public TimeManager.DateTime SummerSolstice(int year)
        {
            if (year == 0) year = 1;
            return new TimeManager.DateTime(28,(int)Season.SecondMonth,year,6,0);
        }
        public TimeManager.DateTime PumpkinHarvest(int year)
        {
            if (year == 0) year = 1;
            return new DateTime(28, (int)Season.ThirdMonth, year, 6, 0);
        }

        #endregion
        #region Start of Month


        public TimeManager.DateTime StartOfMonth(int season,int year)
        {
            return new DateTime(1, (int)season, year, 6, 0);   
        }
        #endregion

        #region To Strings
        public override string ToString()
        {
            return $"Date: {DateToString()} " +
                $"Month: {season}" +
                $"Time: {TimeToString()}" +
                $"\nTotal Days: {totalNumDays} | " +
                $"Total Weeks: {totalNumWeeks}";
        }

        public string DateToString()
        {
            return $"{Day} {Date} {Year.ToString("D2")}";
        }

        public string TimeToString()
        {
            int adjustedHour = 0;
            
            if (hour == 0 || adjustedHour == 24)
            {
                adjustedHour = 12;
            }
            else if (hour >=13)
            {
                adjustedHour = hour - 12;
            }
            else
            {
                adjustedHour = hour;
            }
            string AmPM = hour == 0 || hour < 12 ? "AM" : "PM";
            return $"{adjustedHour.ToString("D2")}:{minutes.ToString("D2")} {AmPM}";
        }
        #endregion


        //[System.Serializable]
        public enum Days
        {
            NULL = 0,
            Monday = 1,
            Tuesday = 2,
            Wednesday = 3,
            Thursday = 4,
            Friday = 5,
            Saturday = 6,
            Sunday = 7
        }

        //[System.Serializable]
        public enum Season
        {
            FirstMonth = 0,
            SecondMonth = 1,
            ThirdMonth = 2 ,
            ForthMonth = 3
        }
    }
}
