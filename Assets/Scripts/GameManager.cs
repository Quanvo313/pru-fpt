using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public int curDay
    {
        get
        {
            return GameObject.FindGameObjectWithTag("TimeManager").GetComponent<TimeManager>().GetTotalDays();
        }
    }
    public int money;
    public Dictionary<CropData, int> cropInventory;
    public Dictionary<CropData,int> produceInventory;


    public CropData selectedCropToPlant;

    #region Class setup
    // Singleton
    public static GameManager instance;

    void OnEnable()
    {
        Crop.OnPlantCrop += OnPlantCrop;
        Crop.OnHarvestCrop += OnHarvestCrop;
    }
    void OnDisable()
    {
        Crop.OnPlantCrop -= OnPlantCrop;
        Crop.OnHarvestCrop -= OnHarvestCrop;
    }

    void Awake()
    {
        // Initialize the singleton.
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }

    #endregion


    #region Crop growing
    // Called when a crop has been planted.
    // Listening to the Crop.onPlantCrop event.
    public void OnPlantCrop(CropData crop)
    {
        cropInventory.Add(crop, cropInventory[crop] - 1);
    }
    // Called when a crop has been harvested.
    // Listening to the Crop.onCropHarvest event.
    public void OnHarvestCrop(CropData crop)
    {
        int inventoryCount;
        if (produceInventory.TryGetValue(crop, out inventoryCount) is false)
        {
            produceInventory.Add(crop, 1);
        }
        else
        {
            produceInventory[crop] = inventoryCount + 1;
        }
    }
    // Called when we want to purchase a crop.
    public void PurchaseCrop(CropData crop)
    {
        Crop croplol = new Crop();
    }
    // Do we have enough crops to plant?
    public bool CanPlantCrop()
    {
        return false;
    }
    // Called when the buy crop button is pressed.
    public void OnBuyCropButton(CropData crop)
    {
    }
    #endregion
}
