using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting.Antlr3.Runtime;
using UnityEngine;
using UnityEngine.Events;

public class Crop : MonoBehaviour
{
    /// <summary>
    /// General data for this crop type
    /// </summary>
    [SerializeField]
    private CropData CropType;

    /// <summary>
    /// The day this crop is planted
    /// </summary>
    private int PlantedDay;
    /// <summary>
    /// Tracks watered state of this crop on daily reset
    /// </summary>
    private bool IsWatered;
    /// <summary>
    /// Tracks current growth state
    /// </summary>
    private int CurrentStage;
    /// <summary>
    /// Tracks total number of days not watered
    /// </summary>
    private int DaysNotWatered;

    /// <summary>
    /// Multi-harvest crops only
    /// </summary>
    private int DayLastHarvested;


    public SpriteRenderer Renderer;

    //Events
    public static event UnityAction<CropData> OnPlantCrop;
    public static event UnityAction<CropData> OnHarvestCrop;

    #region Crop status

    // Called when a new day ticks over.
    public void NewDayCheck()
    {
        int cropProgress = CropProgress();
        int stageCheck = 0;
        for  (int i = 0; i < CropType.StagesDuration.Length; i++)
        {
            int stageDuration = CropType.StagesDuration[i];
            stageCheck += stageDuration;
            if (stageCheck >= cropProgress)
            {
                CurrentStage = i;
                break;
            }
        }
        UpdateCropSprite();
    }
    /// <summary>
    /// Update the crop sprite based on growth stage
    /// </summary>
    void UpdateCropSprite()
    {
        //int cropProg = CropProgress();
        if (IsWatered is false)
        {
            DaysNotWatered++;
        }
        if (CanHarvest())
        {
            Renderer.sprite = CropType.readyToHarvestSprite;
        }
        else if (IsWatered is true)
        {
            Renderer.sprite = CropType.StagesSprites[CurrentStage];
        }
        
        IsWatered = false;
        //if (cropProg < curCrop.daysToGrow)
        //{
        //    sr.sprite = curCrop.growProgressSprites[cropProg];
        //}
        //else
        //{
        //    sr.sprite = curCrop.readyToHarvestSprite;
        //}
    }

    #endregion


    #region Crop interaction

    // Called when the crop has been planted for the first time.
    public void Plant()
    {
        PlantedDay = GameManager.instance.curDay;
        IsWatered = false;
        UpdateCropSprite();
        OnPlantCrop?.Invoke(CropType);
        CurrentStage = 0;
        DaysNotWatered = 0;
        DayLastHarvested = -1;
        TimeManager.OnNewDay += NewDayCheck;
    }

    /// <summary>
    /// Water the crop
    /// </summary>
    public void Water()
    {
        IsWatered = true;
    }
    /// <summary>
    /// Called when we want to harvest the crop.
    /// </summary>
    public void Harvest()
    {
        if (CanHarvest())
        {
            OnHarvestCrop?.Invoke(CropType);
            if (CropType.isMultiHarvest is false)
                Destroy(gameObject);//Destroy if crop is single-harvest
            else
            {
                DayLastHarvested = GameManager.instance.curDay;
            }
        }
    }
    #endregion

    #region Crop info
    /// <summary>
    /// <returns>Days grown</returns>
    /// </summary>
    // Returns the number of days that the crop has been planted for.
    int CropProgress()
    {
        if (CropType.isMultiHarvest is true && DayLastHarvested is not -1)
        {
            return GameManager.instance.curDay - DayLastHarvested;
        }
        return GameManager.instance.curDay - PlantedDay - DaysNotWatered;
    }

    /// <summary>
    /// Check if harvestable
    /// </summary>
    /// <returns></returns>
    public bool CanHarvest()
    {
        if (CropType.isMultiHarvest is false)
            return CropProgress() >= CropType.daysToGrow;//Single harvest crop
        else
            return IsMature() && CropProgress() % CropType.RegrowDay == 0;//Multi-harvest crops
    }

    /// <summary>
    /// Check if crop is mature
    /// </summary>
    /// <returns></returns>
    public bool IsMature()
    {
        if (CropType.isMultiHarvest is true && DayLastHarvested is not -1)
            return true;

        return CropProgress() >= CropType.daysToGrow;
    }
    #endregion
}
