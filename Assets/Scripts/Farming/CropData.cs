using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Crop Data", menuName = "New Crop Data")]
public class CropData : ScriptableObject
{
    [Header("Appearance")]
    [SerializeField]
    public string SeedName;
    [SerializeField]
    public Sprite SeedSprite;
    [SerializeField]
    public string ProduceName;
    [SerializeField]
    public Sprite[] StagesSprites;
    [SerializeField]
    public int[] StagesDuration;
    [SerializeField]
    public int daysToGrow
    {
        get
        {
            int days = 0;
            foreach (var stage in StagesDuration)
            {
                days+=stage;
            }
            return days;
        }
    }
    [SerializeField]
    public Sprite readyToHarvestSprite;

    [Header("Game mechanics info")]
    [SerializeField]
    public TimeManager.DateTime.Season[] GrowableMonths;
    [SerializeField]
    [Range(1,3000)]
    public int purchasePrice;
    [SerializeField]
    [Range(1, 3000)]
    public int sellPrice;
    [SerializeField]
    public bool isMultiHarvest = false;
    [SerializeField]
    public int RegrowDay = -1;
    [SerializeField]
    public int[] produceYield = new int[2] {1,1};
}
