using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class PlayerTileInteraction : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            KeyDownZ();
        }
    }

    private void KeyDownZ()
    {
        // Add your desired functionality here.
        // For example, you can perform an action when the "Z" key is pressed.
        Debug.Log("Z key was pressed.");
        GameObject farmableGroundObject = GameObject.FindGameObjectWithTag("FarmableGround");
        var farmableGroundScript = farmableGroundObject.GetComponent<FarmableGround>();
        if (farmableGroundScript != null)
        {
            // Get the FarmableGround script component attached to the Tilemap game object.
            //FarmableGround farmableGround = farmableGroundObject.GetComponent<FarmableGround>();

            // Invoke the Interact function on the FarmableGround script.
            farmableGroundScript.Interact();
        }
    }

}
