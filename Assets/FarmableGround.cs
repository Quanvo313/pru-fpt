using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FarmableGround : MonoBehaviour
{
    SpriteRenderer sr;
    bool tilled;
    Crop curCrop;
    Sprite tilledSprite;
    Sprite wateredTilledSprite;


    // Called when the player interacts with the tile.
    public void Interact()
    {
        Console.WriteLine("Interacted");
        //if (!tilled)
        //{
        //    Till();
        //}
        //else if (!HasCrop() && GameManager.instance.CanPlantCrop())
        //{
        //    PlantNewCrop(GameManager.instance.selectedCropToPlant);
        //}
        //else if (HasCrop() && curCrop.CanHarvest())
        //{
        //    curCrop.Harvest();
        //}
        //else
        //{
        //    Water();
        //}
    }

    // Called when we interact with a grass tile.
    void Till()
    {
        tilled = true;
        sr.sprite = tilledSprite;
    }
    // Called when we interact with a crop tile.
    void Water()
    {
        sr.sprite = wateredTilledSprite;
        if (HasCrop())
        {
            curCrop.Water();
        }
    }

    bool HasCrop()
    {
        return false;
    }

    void PlantNewCrop(CropData cropToPlant)
    {

    }
}
